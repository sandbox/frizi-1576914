/* @file this js file contains script for handling dynamic grid content - Drupal dynamic grid module
 * see: http://drupal.org/sandbox/frizi/1576914
 * */

// create two dimensional array function

function Create2DArray(rows) {
  var arr = [];
  for (var i=0;i<rows;i++) {
     arr[i] = [];
  }
  return arr;
}

(function ($) {
    $(document).ready(function() {
    	
    	// init the grid size array
    	var cell_arr = new Array();
    	cell_arr[3] = 296;
    	cell_arr[4] = 216;
    	cell_arr[5] = 168;
    	cell_arr[6] = 136;
    	cell_arr[8] = 96;    	
    	
    	var width_img_arr = Create2DArray(15);
    	
    	// init the image array
		width_img_arr[3][1] = 296;
		width_img_arr[3][2] = 614;
		width_img_arr[3][3] = 614;
		
		width_img_arr[4][1] = 216;
		width_img_arr[4][2] = 456;
		width_img_arr[4][3] = 696;
		
		width_img_arr[5][1] = 168;
		width_img_arr[5][2] = 552;
		width_img_arr[5][3] = 744;
		
		width_img_arr[6][1] = 136;
		width_img_arr[6][2] = 616;
		width_img_arr[6][3] = 456;
		
		width_img_arr[8][1] = 216;
		width_img_arr[8][2] = 456;
		width_img_arr[8][3] = 336;
		
    	gen_grid($('.grid_size').find("option:selected").val());
    	$('#gcont').toggle('2000');
    	
		$('link').remove();
		$('head').append('<link rel="stylesheet" type="text/css" href="/' + Drupal.settings.dynagrid.module_path + '/css/dynagrid.css">'); 
		
		// function that generates the overlay grid via HTML div and css manipulation
		function gen_grid( grid_size, grid_lenght ){
			if($('.grid').length > 0){
				$('.grid').empty();
				for(var i = 1 ; i < grid_size*190*3+1 ; i++){
		   			if(i%3 !=2){
						$('.grid').append('<div class="margin cell"></div>');
					} 
					else{
						$(".grid").append('<div class="body cell"></div>');
					}
				}
	    		$('.grid .cell.body').css('width', cell_arr[grid_size] + 'px');
			}
		}
		// on click show hide grid button handler
		$('.sh_grid').click(function() {
			  $('.grid').toggle('fast');
		});
		// on click show / hide setting button handler
		$('#gcont_endis').click(function() {
			  $('#gcont').toggle('slow');
		});
		
		// function to change the width of the images - change source
		function change_width(grid_size){
			
			var first_row_width_arr = $('.img_two img').attr("src").split('/');
			first_row_width_arr[2] = width_img_arr[grid_size][1];
			$('.img_two img').attr("src", first_row_width_arr.join('/'));
			
			var second_row_width_arr = $('.img_three img').attr("src").split('/');
			second_row_width_arr[2] = width_img_arr[grid_size][2];
			$('.img_three img').attr("src", second_row_width_arr.join('/'));
			
			var third_row_width_arr = $('.img_four img').attr("src").split('/');
			third_row_width_arr[2] = width_img_arr[grid_size][3];
			$('.img_four img').attr("src", third_row_width_arr.join('/'));
		}
    	
		// the css textarea builder function
		function build_css(){ 
			temp_val = "*{\n\tmargin: 0px;\n\tpadding: 0px;\n}\nbody{\tfont-size: "+$('body').css('font-size');
			temp_val += ";\n\tbackground-color: #fff;\n\ttext-align: left;\n\tline-height: "+$('body').css('line-height');
			temp_val += ";\n\tcolor: #000;\n\tletter-spacing: "+$('body').css('letter-spacing')+";\nfont-family: "+$('body').css('font-family')+";";
			temp_val += "\n}\na{\n\ttext-decoration: none;\n\tcolor: #494949;\n\tborder-bottom: 1px dashed #ccc;\n}\n";
			temp_val += "a:hover{\n\tcolor: #000;\n\tborder-bottom: 1px dotted #000;\n}\nimg{\n\tdisplay: block;\n\tborder: 0px;\n}";
			
			$('.result').val(temp_val);
		}
		// font face change function
    	$('.font_face').change(function() {
    		$('body').css('font-family', font_faces($(this).find("option:selected").val()));
    		build_css();    		
    	});
    	// font size handler function
    	$('.font_size').change(function() {
   			$('body').css('font-size', $(this).find("option:selected").val() + 'px');
   			build_css();

    	});
    	// line height handler function
    	$('.line_height').change(function() {
    		$('body').css('line-height', $(this).find("option:selected").val() + 'px');
    		$('.grid .cell').css('height', $(this).find("option:selected").val() + 'px');
    		$('.grid .cell').css('line-height', $(this).find("option:selected").val() + 'px');
    		
    		$('.img').css('margin-top', $(this).find("option:selected").val() + 'px');
    		$('.img').css('margin-bottom', $(this).find("option:selected").val() + 'px');
    		
    		$('h1, h2, h3, h4, h5, h6, h7').css('padding-top', $(this).find("option:selected").val() + 'px');
    		
    		var banner_row_arr = $('.img_one img').attr("src").split('/');
    		banner_row_arr[3] = $(this).find("option:selected").val() * 10;
    		$('.img_one img').attr("src", banner_row_arr.join('/'));
    		
    		var first_row_image_arr = $('.img_two img').attr("src").split('/');
    		first_row_image_arr[3] = $(this).find("option:selected").val() * 10;
    		$('.img_two img').attr("src", first_row_image_arr.join('/'));
    		
    		var second_row_image_arr = $('.img_three img').attr("src").split('/');
    		second_row_image_arr[3] = $(this).find("option:selected").val() * 10;
    		$('.img_three img').attr("src", second_row_image_arr.join('/'));
    		
    		var third_row_image_arr = $('.img_four img').attr("src").split('/');
    		third_row_image_arr[3] = $(this).find("option:selected").val() * 20;
    		$('.img_four img').attr("src", third_row_image_arr.join('/'));
    		build_css();
    		
    	});
    	// letter space setting handler function
    	$('.letter_space').change(function() {
   			$('body').css('letter-spacing', $(this).find("option:selected").val());
   			build_css();
    	});
    	// grid opacity change event
    	$('.grid_opa').change(function() {
   			$('.grid .cell.margin').css('opacity', '.'+ (+$(this).find("option:selected").val() + 1) );
   			$('.grid .cell.margin').css('filter', 'alpha(opacity=' + (+$(this).find("option:selected").val() + 1) + '0)');
   			$('.grid .cell.body').css('opacity', '.'+$(this).find("option:selected").val());
   			$('.grid .cell.body').css('filter', 'alpha(opacity='+$(this).find("option:selected").val() + '0)');
   			build_css();
    	});
    	// grid size handling
    	$('.grid_size').change(function() {
    		$('.grid').show('fast');
    		build_css();
    		gen_grid($(this).find("option:selected").val());
    		$('.grid .cell').css('height', $('.line_height').find("option:selected").val() + 'px');
    		$('.grid .cell').css('line-height', $('.line_height').find("option:selected").val() + 'px');
    		change_width($(this).find("option:selected").val());
    	});
    	
    	// content align radio button change setting
		$("input[name='main_align']").change(function(){
			if ($("input[name='main_align']:checked").val() == 'left') {
				$('#grid_content').css('margin', '40px auto 20px 0px');
			}
			else if ($("input[name='main_align']:checked").val() == 'center') {
				$('#grid_content').css('margin', '40px auto 20px auto');
			}
			else if ($("input[name='main_align']:checked").val() == 'right') {
				$('#grid_content').css('margin', '40px 0px 20px auto');
			}
		});
		
		// after each image objects loaded adjust the grid height
		$('.img img').load(function() {
			$('.grid').css('height', $('#content_wrap').css('height'));
			$('.font_face, .font_size, .line_height, .letter_space, .grid_size').change(function() {
				$('.grid').css('height', $('#content_wrap').css('height'));
			});
		});

		$('head').append(' <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine|Handlee">');
    });
}(jQuery));

// default font faces - extend by your own
function font_faces(font_code){
	var font_faces = new Array();
	font_faces['f1'] = '"Arial", "Helvetica", sans-serif';
	font_faces['f2'] = '"Arial Black", "Gadget", sans-serif';
	font_faces['f3'] = '"Impact", "Charcoal", sans-serif';
	font_faces['f4'] = '"Lucida Sans Unicode", "Lucida Grande", sans-serif';
	font_faces['f5'] = '"Tahoma", "Geneva", sans-serif';
	font_faces['f6'] = '"Trebuchet MS", "Helvetica", sans-serif';
	font_faces['f7'] = '"Verdana", "Geneva", sans-serif';
	font_faces['f8'] = '"Palatino Linotype", "Palatino", "Book Antiqua", serif';
	font_faces['f9'] = '"Times New Roman", "Times", serif';
	font_faces['f10'] = '"Georgia", serif';
	font_faces['f11'] = '"Comic Sans MS", cursive';
	font_faces['f12'] = '"Courier New", monospace';
	font_faces['f13'] = '"Lucida Console", "Monaco", monospace';

	font_faces['f14'] = '"Tangerine", serif';
	font_faces['f15'] = '"Handlee", cursive';

	return(font_faces[font_code]);
}

