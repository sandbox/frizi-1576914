
<div id="gcont-wrapper">
<div id="gcont_endis">Show / Hide Settings</div>

 <!-- grid control -->
  <div id="gcont">
  <p>Content align:</p><br/>
  <input type="radio" name="main_align" value="left"  />&nbsp;&nbsp;
  <input type="radio" name="main_align" value="center"  />&nbsp;&nbsp;
  <input type="radio" name="main_align" value="right"  /><br />
    <select class="font_face contr" name="font_face">
      <option value="none" selected="selected">Font Family&nbsp;</option>
      <optgroup label="sans-serif">
        <option value="f1" selected="selected">Arial / Helvetica</option>
        <option value="f2">Arial Black / Gadget</option>
        <option value="f3">Impact / Charcoal</option>
        <option value="f4">Lucida Sans Unicode / Grande</option>
        <option value="f5">Tahoma / Geneva</option>
        <option value="f6">Trebuchet MS / Helvetica</option>
        <option value="f7">Verdana / Geneva</option>
      </optgroup>
      <optgroup label="serif">
        <option value="f8">Palatino Linotype / Book Antiqua</option>
        <option value="f9">Times New Roman / Times</option>
        <option value="f10">Georgia</option>
      </optgroup>
      <optgroup label="cursive">
        <option value="f11">Comic Sans MS</option>
      </optgroup>
      <optgroup label="monospace">
        <option value="f12">Courier New</option>
        <option value="f13">Lucida Console / Monaco</option>
      </optgroup>
      <optgroup label="Google webfont">
        <option value="f14">Tangerine</option>
        <option value="f15">Handlee</option>
      </optgroup>
    </select>
    
    <br />
    <select class="font_size contr" name="font_size">
      <option value="none" selected="selected">Font size&nbsp;</option>
      <option value="8">8 px</option>
      <option value="9">9 px</option>
      <option value="10">10 px</option>
      <option value="11">11 px</option>
      <option value="12">12 px</option>
      <option value="13">13 px</option>
      <option value="14">14 px</option>
      <option value="15">15 px</option>
      <option value="16">16 px</option>
      <option value="17">17 px</option>
      <option value="18">18 px</option>
      <option value="19">19 px</option>
      <option value="20">20 px</option>
      <option value="21">21 px</option>
      <option value="22">22 px</option>
      <option value="23">23 px</option>
      <option value="24">24 px</option>
    </select>
    <br />
    <select class="line_height contr" name="line_height">
      <option value="none" selected="selected">Line height&nbsp;</option>
      <option value="14">14 px</option>
      <option value="16">16 px</option>
      <option value="18">18 px</option>
      <option value="20">20 px</option>
      <option value="22">22 px</option>
      <option value="24">24 px</option>
      <option value="26">26 px</option>
      <option value="28">28 px</option>
      <option value="30">30 px</option>
    </select>
    <br />
    <select class="letter_space contr" name="letter_space">
      <option value="none" selected="selected">Letter spacing&nbsp;</option>
      <option value="0px">0 px</option>
      <option value="1px">1 px</option>
      <option value="2px">2 px</option>
      <option value="3px">3 px</option>
    </select>
    <br />
    <select class="grid_opa contr" name="grid_opa">
      <option value="5" selected="selected">Grid Opacity&nbsp;</option>
      <option value="2">20 %</option>
      <option value="3">30 %</option>
      <option value="4">40 %</option>
      <option value="5">50 %</option>
      <option value="6">60 %</option>
      <option value="7">70 %</option>
      <option value="8">80 %</option>
    </select>
    <br />
    <select class="grid_size contr" multiple="multiple" rows="20">
      <option value="3"> 3x grid</option>
      <option value="4" selected="selected"> 4x grid&nbsp;</option>
      <option value="5"> 5x grid</option>
      <option value="6"> 6x grid</option>
      <option value="8"> 8x grid</option>
    </select>
    <br />
    <button class="sh_grid contr" type="button">&nbsp;Show/hide grid&nbsp;</button> 
    <br /><br />
    <textarea class="result contr" cols="43" rows="26"><?php echo "*{\n\tmargin: 0px;\n\tpadding: 0px;\n}\nbody{
  \tfont-size: 12px;\n\tbackground-color: #fff;\n\ttext-align: left;\n\tline-height: 20px;\n\tcolor: #000;
  \tletter-spacing: 0px;\n\tfont-family: \"Arial\", sans-serif;\n}
  a{\n\ttext-decoration: none;\n\tcolor: #494949;\n\tborder-bottom: 1px dashed #ccc;\n}
  a:hover{\n\tcolor: #000;\n\tborder-bottom: 1px dotted #000;\n}
  img{\n\tdisplay: block;\n\tborder: 0px;\n}"?>
    </textarea> 
  </div>
</div>
<!-- // grid control -->

<div id="grid_content">
  <div class="grid"></div>
  <div id="content_wrap">
    <div class="text_row">
      <p>abcdefghijklmnoprstuvxyz -+*/-1234567890.!@#£$%^&*()_+""''|{}?><~</p>
      <p>a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  r  s  t  u  v  x  y  z  -  +  *  /  -  1  2  3  4  5  6  7  8  9  0  .  !  @  #  £  $  %  ^  &  *  (  )  _  +  "  "  '  '  |  {  }  ?  >  <  ~  </p>
      <p>A B C D E F G H I J K L M N O P R S T U V X Y Z   Ö Ä Ü ß</p>
      <p> ľ š č ť ž ý á í é ä ú ô ň</p>
      <p> Ľ Š Č Ť Ž Ý Á Í É Ä Ú Ô Ň</p>
      <p>а б в г д е ж ѕ z з и і к л м н о п р с т ȣ ѹ ф х ѿ ц ч ш щ ъ Ꙑ ь ѣ Ꙗ ѥ ю ѧ ѫ ѩ ѭ ѡ ѻ ѯ ѱ ѳ ѵ</p>
      <p>А Б В Г Д Е Ж Ѕ Z З И І К Л М Н О П Р С Т Ȣ Ѹ Ф Х Ѿ Ц Ч Ш Щ Ъ Ꙑ Ь Ѣ Ꙗ Ѥ Ю Ѧ Ѫ Ѩ Ѭ Ѡ Ѻ Ѯ Ѱ Ѳ Ѵ</p>
      <p><em>This is an '&lt;em&gt;' tag.</em></p>
      <p><b>This sentence is written in bold.</b></p>
      <p><i>Any need of italic text? here you go!</i></p>
      <p><strike>Just a very simple sentence design example.</strike></p>
      <p><u>THIS SENTENCE IS A CAPITAL EXAMPLE, VERY NICE!</u></p>
    <div class="clear_all"></div>

    </div>
    <div id="main_content">
      <div class="img left img_one"><img src="<?php print $images_arr['four']; ?>" alt="First image" /></div>
      <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur. <a href="#">Click here</a></p>
      <div class="img left img_two"><img src="<?php print $images_arr['one']; ?>" alt="First image" /></div>
      <h2>First title</h2><p>Nam sagittis semper mollis. <i>Fusce lobortis sollicitudin purus eget dignissim.</i> <b><i>Sed id purus sapien.</i></b> 
      Nunc rhoncus lacinia diam, sed interdum sapien varius vitae. In commodo fermentum ornare. Suspendisse rhoncus, urna. </p>
      <p>Nam vestibulum faucibus placerat. Pellentesque eu quam vitae elit facilisis mattis. In pharetra diam eu orci imperdiet et rutrum quam semper. 
      Donec dolor nisl, venenatis nec condimentum eget, venenatis sed. </p>
      <p>Maecenas posuere lorem ut lacus fermentum lacinia. Mauris sagittis sapien id sapien fermentum vitae molestie purus hendrerit. Mauris faucibus, 
      lectus vel fermentum adipiscing, enim lacus volutpat nibh, eget porta arcu.</p>
      <p>Vivamus vulputate dignissim viverra. Vivamus pretium arcu varius libero gravida vel dapibus quam tempus. Integer blandit volutpat arcu. Quisque 
      aliquet tellus nec sapien consectetur iaculis. Nullam facilisis eleifend diam, nec.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla suscipit lectus eu mauris scelerisque viverra. Donec commodo ullamcorper interdum. 
      Etiam non leo erat, eu tempor. </p>
      
      <p>Duis viverra, odio ac interdum semper, dolor massa eleifend eros, vel accumsan elit orci in nisi. Vivamus hendrerit 
      leo auctor urna blandit eu euismod mi sollicitudin. Curabitur sit amet ornare.  </p>
      <p>Sed tortor quam, commodo eu gravida et, pulvinar pretium orci. Sed tristique lectus ac tortor bibendum accumsan. Aliquam euismod turpis ac quam
       consequat tempus. Suspendisse faucibus turpis sed quam auctor. Sed velit metus, consequat a suscipit eu.</p>
      <div class="img right img_three"><img src="<?php print $images_arr['two']; ?>" alt="First image" /></div>
      <p>Proin dignissim, mauris a euismod malesuada, elit purus malesuada purus, in dapibus lectus leo eu tellus. Cras imperdiet hendrerit tristique.</p>
      <h3>The next title</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at nulla vel dui scelerisque luctus id et nisi. Phasellus porta suscipit sapien. 
      Praesent vestibulum, quam. </p>
      
      <div class="img left img_four"><img src="<?php print $images_arr['three']; ?>" alt="First image" /></div>
      
      <p>Donec in pellentesque libero. Cras ut felis ipsum. Sed in sem tortor, in sollicitudin ipsum. Nunc eros dui, porta nec 
      convallis ut, bibendum in leo. Donec molestie ante in nunc. </p>
      <p>Praesent ultricies, libero at dapibus commodo, nulla libero rhoncus lacus, eget porttitor nisi velit eu nibh. Nullam scelerisque scelerisque ipsum 
      at convallis. Donec iaculis eros ipsum, sit amet mattis velit. </p>
      <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>
      <p>Curabitur consequat venenatis lorem dapibus venenatis. In hac habitasse platea dictumst. Aliquam venenatis leo ut massa vulputate sollicitudin.
       Proin a lacus nec neque scelerisque rutrum. Sed malesuada erat magna. Duis non aliquam. </p>
      <p>Donec quis semper sem. Proin a tortor. Aliquam eleifend odio vel orci posuere id iaculis lorem fermentum. Nullam pharetra, urna quis pharetra 
      consectetur, sem enim imperdiet orci, vitae mollis purus purus non. </p>
      <p>Sed velit metus, consequat a suscipit eu. Morbi in erat sit amet metus euismod pharetra id ut lacus. Pellentesque faucibus aliquam tellus nec 
      aliquet. Donec vitae tortor congue metus auctor fringilla. Ut. </p>
      <p>Integer tellus enim, vulputate ac fringilla sed, ullamcorper ac odio. Phasellus eu lorem vestibulum turpis fermentum luctus in vel mi. Donec vitae quam elit. Nam. 
      Nunc arcu sapien, auctor vel viverra ut, iaculis quis nisi. Maecenas nec diam eu elit consequat sodales. </p>
      <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sed erat a mauris venenatis interdum id ut lectus. In hac. 
      Nunc vulputate semper neque varius interdum. Praesent quis odio magna. In et dui nisl, nec interdum libero.
      Praesent ultricies, libero at dapibus commodo, nulla libero rhoncus lacus, eget porttitor nisi velit eu nibh. 
      Maecenas nec diam eu elit consequat sodales.</p>

      <div class="clear_all"></div>
      <br />
      <div id="bottom">
        <div class="headings column">
          <h1>Heading 1</h1>
          <h2>Heading 2</h2>
          <h3>Heading 3</h3>
          <h4>Heading 4</h4>
          <h5>Heading 5</h5>
          <h6>Heading 6</h6>
        </div>
        
        <div class="text_ul column">
          <ul>
            <li><i>First li</i></li>
            <li>Second li</li>
            <li><b>Third li</b></li>
            <li><i>Fourth li</i></li>
          </ul>
        </div>
        
        <div class="text_ol column">
        <ol>
          <li>First li</li>
          <li>Second li</li>
          <li><b>Third li</b></li>
          <li><i>Fourth li</i></li>
        </ol>
        </div>
      </div>
       <div class="clear_all"></div>
  </div>
</div>
</div>
